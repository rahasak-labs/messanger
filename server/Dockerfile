FROM golang:1.15

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install packages
RUN apt-get update
RUN apt-get install unzip

# install protoc
env PROTOC_ZIP=protoc-3.7.1-linux-x86_64.zip
RUN curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.7.1/$PROTOC_ZIP
RUN unzip -o $PROTOC_ZIP -d /usr/local bin/protoc
RUN unzip -o $PROTOC_ZIP -d /usr/local 'include/*'
RUN rm -f $PROTOC_ZIP

# install go dependencies
RUN go get -u google.golang.org/grpc
RUN go get -a github.com/golang/protobuf/protoc-gen-go

# copy app
ADD . /app
WORKDIR /app

# add protobuf spec
RUN git clone https://gitlab.com/rahasak-labs/messanger.git
RUN protoc messanger/spec/*.proto --go_out=plugins=grpc:$GOPATH/src
RUN rm -rf messanger

# build
RUN go build -o build/messanger src/*.go

# server running port
EXPOSE 9000

ENTRYPOINT ["/app/docker-entrypoint.sh"]
