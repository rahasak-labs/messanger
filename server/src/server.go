package main

import (
	"log"

	"gitlab.com/rahasaklabs/messanger/spec"
	"golang.org/x/net/context"
	"io"
	"time"
)

type Server struct {
}

func (s *Server) SendMessage(ctx context.Context, msg *spec.Message) (*spec.Reply, error) {
	log.Printf("receive message: %s", msg)
	return &spec.Reply{Id: msg.Id, Status: "OK"}, nil
}

func (s *Server) StreamMessage(stream spec.MessageService_StreamMessageServer) error {
	// read client message
	go func() {
		for {
			msg, err := stream.Recv()
			if err == io.EOF {
				log.Printf("client write closed, %s", err)
				break
			}

			if err != nil && err != io.EOF {
				log.Printf("error reading from client, %s", err)
				break
			}

			log.Printf("message from client, %s", msg)
		}
	}()

	// send periodic message to client
	for {
		log.Printf("send message to client")
		time.Sleep(1 * time.Second)

		// check client alive
		err := stream.Context().Err()
		if err != nil {
			log.Printf("error stream on client, %s", err)
			break
		}

		err = stream.Send(&spec.Message{Id: "1", Body: "rahasak labs"})
		if err != nil {
			log.Printf("error sending message to client, %s", err)
			break
		}
	}

	return nil
}

func (s *Server) ServeMessage(msg *spec.Message, stream spec.MessageService_ServeMessageServer) error {
	log.Printf("receive message: %s", msg)

	// send periodic message to client
	for {
		log.Printf("send message to client")
		time.Sleep(1 * time.Second)

		// check client alive
		err := stream.Context().Err()
		if err != nil {
			log.Printf("error stream on client, %s", err)
			break
		}

		err = stream.Send(&spec.Message{Id: "1", Body: "rahasak labs"})
		if err != nil {
			log.Printf("error sending message to client, %s", err)
			break
		}
	}

	return nil
}
